package main

import (
	"fmt"
	"os"
	"os/exec"
	"time"
)

func main() {
	for {
		cmd := exec.Command("clear")
		cmd.Stdout = os.Stdout
		cmd.Run()
		fmt.Println(time.Now().Format("2006-01-02 15:04:05"))
		time.Sleep(1 * time.Second)
	}
}
